import Vue from 'vue';
import Vuex from 'vuex';
import stocks from '@/store/stock';

Vue.use(Vuex);

export default new Vuex.Store({
    ...stocks
});