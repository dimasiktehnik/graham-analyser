import actions from './actions';
import Vue from 'vue';

export default {
    actions: { ...actions },
    state: {
        stocks: {},
        favorites: [],
        activeStock: null,
    },
    mutations: {
        addStockDataToList(state, data) {
            Vue.set(state.stocks, data.symbol, data);
        },

        removeStockFromList(state, symbol) {
            Vue.delete(state.stocks, symbol);
        },

        setActiveStock(state, symbol) {
            state.activeStock = state.stocks[symbol];
        },

        addStockToFavorites(state, symbol) {
            state.favorites.push(symbol);
        },

        removeStockFromFavorites(state, symbol) {
            const index = state.favorites.indexOf(symbol);

            state.favorites.splice(index, 1);
        },
    },
    getters: {}
}