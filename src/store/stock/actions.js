import {
    getSearchResults,
} from '@/api';
// import { mockedData } from '@/api/static/mocked-data';

import StockMapper from '@/api/services/StockMapper.js';
import { syncFavoritesStocksWithLocalStorage } from '@/api/helpers';

export default {
    async getStockBySymbol({ commit }, symbol) {
        // // if trouble will appear with some endpoint -- show error and return;
        const stockMapper = new StockMapper(symbol);
        const stock = await stockMapper.initialize();
        //todo remove after train
        // Object.values(mockedData).forEach(stock => commit('addStockDataToList', stock))

        commit('addStockDataToList', stock);
    },

    setActiveStock({ commit }, symbol) {
        commit('setActiveStock', symbol);
    },

    removeStockFromList({ commit, dispatch }, symbol) {
        commit('removeStockFromList', symbol);
        dispatch('syncStocksWithFavorites', { symbol, action: 'remove' });
    },

    syncStocksWithFavorites({ commit, state }, { symbol, action }) {
        const localStorageListKey = 'favorites'

        if (action === 'add') { 
            commit('addStockToFavorites', symbol);
        }

        if (action === 'remove') { 
            commit('removeStockFromFavorites', symbol);
        }

        syncFavoritesStocksWithLocalStorage(localStorageListKey, JSON.stringify(state.favorites));
    },

    async searchCompanies({}, keywords) {
        const { bestMatches: data } = await getSearchResults(keywords);

        return data.filter(company => company['4. region'] === 'United States' && company['3. type'] === 'Equity');
    }
}