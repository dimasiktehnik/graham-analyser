import http from '@/api/settings/http.js';

export async function getOverview(symbol) {
    return await http.get({ function: 'OVERVIEW', symbol });
};

export async function getBalanceSheet(symbol) {
    return await http.get({ function: 'BALANCE_SHEET', symbol });
};

export async function getIncomeStatement(symbol) {
    return await http.get({ function: 'INCOME_STATEMENT', symbol });
};

export async function getMonthlySummary(symbol) {
    return await http.get({ function: 'TIME_SERIES_MONTHLY_ADJUSTED', symbol });
};

export async function getEarnings(symbol) {
    return await http.get({ function: 'EARNINGS', symbol });
};

export async function getLastUpdatedPrice(symbol) {
    const data = await http.get({ function: 'TIME_SERIES_INTRADAY', symbol, interval: '5min' });
    const [ price ] = Object.values(data['Time Series (5min)']);

    return {
        lastRefreshed: data['Meta Data']['3. Last Refreshed'],
        price: +Number(price['4. close']).toFixed(2),
    }
}

export async function getSearchResults(keywords) {
    return await http.get({ function: 'SYMBOL_SEARCH', keywords });
}