const headers = {
    'X-RapidAPI-Host': process.env.VUE_APP_HOST,
    'X-RapidAPI-Key': process.env.VUE_APP_API_KEY
}

export { headers };