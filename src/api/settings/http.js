import axios from 'axios';

const url = process.env.VUE_APP_HOST;
const apikey = process.env.VUE_APP_API_KEY;

export default class Http {
    constructor() {
        this.headers = axios.defaults.headers;
    }

    static async get(params) {
        try {
            const response = await axios.get(url, {
                headers: this.headers,
                params: {
                    ...params,
                    apikey
                }
             });

            return response.data;
        } catch(e) {
            throw e;
        }
    }
}