export default {
    danger: { value: 'danger', color: 'red' },
    warning: { value: 'warning', color: 'orange' },
    success: { value: 'success', color: 'green' },
    secondary: { value: 'secondary', color: 'grey' },
}
