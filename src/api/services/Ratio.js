import { 
    getAverageValueByKey, 
    formatNumbersToMillions, 
    formatQuarter 
} from '@/api/helpers';

export default class RatioService {
    static getCurrentRatio(balanceSheet) {
        const { totalCurrentAssets, totalCurrentLiabilities } = balanceSheet.annualReports[0];
        // dish corp
        const currentRatio = +(totalCurrentAssets / totalCurrentLiabilities).toFixed(2);
    
        return isNaN(currentRatio) ? 'Н/З' : currentRatio;
    }

    static getEarningsByQuarter(quarterlyReports) {
        return quarterlyReports.reverse().map(quarter => { 
            return {
                date: quarter.fiscalDateEnding, 
                value: formatNumbersToMillions(quarter.netIncome),
            };
        })
    }
    
    static getLastUnprofitableQuarter(quarterlyReports) {
        return quarterlyReports.find(({ netIncome }) => +netIncome < 0)?.fiscalDateEnding;
    }
    
    static getLastUnprofitableYear(yearlyReports) {
        return yearlyReports.find(({ netIncome }) => +netIncome < 0)?.fiscalDateEnding;
    }
    
    static getDividendHistory(dividendHistory) {
        const dividendsAccordingMonthes = Object.entries(dividendHistory).map(data => {
            const [date, infoByMonth] = data;
    
            return {
                year: date.slice(0, 4),
                dividendAmount: +infoByMonth['7. dividend amount']
            };
        })

        const dividendsAmountByEachYear = dividendsAccordingMonthes.reduce((acc, dataForMonth) => {
            const currentYearInAcc = acc.find(yearInfo => yearInfo && yearInfo.year === dataForMonth.year)
        
            if (!currentYearInAcc) { 
                acc.push({ year: dataForMonth.year, dividendAmount: dataForMonth.dividendAmount })
            } else {
                currentYearInAcc.dividendAmount = +(currentYearInAcc.dividendAmount + dataForMonth.dividendAmount).toFixed(2);
            }
        
            return acc;
        }, []);

        const currentYear = new Date().getUTCFullYear();
        const endDateRange = currentYear - 20;

        const dividendsForLastPeriod = dividendsAmountByEachYear
            .filter(({ year }) => year <= currentYear && year >= endDateRange)
    
        return dividendsForLastPeriod.reverse();
    }
    
    static getEarningsGrowth(earnings) {
        const startDateRange = new Date().getUTCFullYear();
        const endDateRange = startDateRange - 10;
        const formattedEarnings = earnings
            .map(({ fiscalDateEnding, reportedEPS }) => { return { fiscalDateEnding: fiscalDateEnding.slice(0, 4), reportedEPS: Number(reportedEPS) } })
            .filter(({ fiscalDateEnding }) => fiscalDateEnding < startDateRange && fiscalDateEnding >= endDateRange)
            
        const averageFromPeriodEnd = getAverageValueByKey(formattedEarnings.slice(0, 3), 'reportedEPS');
        const averageFromPeriodStart = getAverageValueByKey(formattedEarnings.reverse().slice(0, 3), 'reportedEPS');

    
        return { averageFromPeriodStart, averageFromPeriodEnd, epsByYears: formattedEarnings };
    }
    
    static getGrahamRatio(balanceSheet, sharesOutstanding) {
        const { totalCurrentAssets, totalLiabilities } = balanceSheet.annualReports[0];
        // qualcomm, dish corp
        const GrahamRatio = ((totalCurrentAssets - totalLiabilities) / (+sharesOutstanding)).toFixed(2);
        return isNaN(GrahamRatio) ? 'Н/З' : GrahamRatio;
    }

    static getGrahamNumber(EPS, bookValuePerShare) {
        // boeing, philip morris
        return Math.sqrt(15 * 1.5 * EPS * bookValuePerShare).toFixed(2);
    }
}