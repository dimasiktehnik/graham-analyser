
import statuses from '@/api/static/statuses';
const { success, warning, danger, secondary } = statuses;

export default class StatusService {
    static getLastUpdatedPrice(lastUpdatedPrice) {
        return { 
            status: secondary,
            ratio: `$${lastUpdatedPrice.price}`,
            message: `Останнє оновлення ${lastUpdatedPrice.lastRefreshed}`,
            title: 'Ціна',
            key: 'price'
        }
    }

    static getCurrentRatio(currentRatio) {
        const title = 'Коеф. поточної ліквідності';
        const key = 'currentRatio';
        let status;
        let message;

        const IDEAL_VALUE = 2;
        const MINIMUM_ACCEPTABLE_VALUE = 1;

        if (currentRatio === 'Н/З') {
            status = secondary;
            message = 'Недостатньо інформації для розрахунку';
        } else if (currentRatio < MINIMUM_ACCEPTABLE_VALUE) {
            status = danger;
            message = 'Статистично низьке значення коефіцієнту поточної ліквідності'
        } else if (currentRatio < IDEAL_VALUE) {
            status = warning;
            message = 'Задовільне значення коефіцієнту поточної ліквідності'
        } else {
            status = success;
            message = 'Поточні активи компанії перевищують її поточні зобов\'язання якнайменш вдвічі';
        }

        return { status, ratio: currentRatio, message, title, key };
    }

    static getStableEarnings(lastUnprofitableYear, lastUnprofitableQuarter) {
        const title = 'Стабільний прибуток';
        const key = 'stableEarnings';
        let status;
        let ratio;
        let message;

        if (lastUnprofitableYear) {
            status = danger;
            ratio = lastUnprofitableYear.slice(0, 4);
            message = `Компанiя мала збитковий ${ratio} рiк`;
        } else if (lastUnprofitableQuarter) {
            status = warning;
            ratio = lastUnprofitableQuarter;
            message = `Компанiя мала збитковий квартал - ${lastUnprofitableQuarter}`;
        } else {
            status = success;
            ratio = '✔';
            message = 'Компанія не мала збиткових кварталів на протязі 5 років.';
        }

        return { status, ratio, message, title, key };
    }

    static getDividendHistory(dividendsByEachYear) {
        const title = 'Дивідендна історія';
        const key = 'dividends';
        const yearsWithDividends = dividendsByEachYear.filter(year => year.dividendAmount);
        let status;
        let ratio;
        let message;

        const currentYear = new Date().getUTCFullYear();
        const previousYear = currentYear - 1;
        const hadLastYearPayment = yearsWithDividends.find(({ year }) => +year === currentYear || +year === previousYear);
        const IDEAL_DIVIDENDS_PAYMENT_RANGE = 20;

        if (!yearsWithDividends.length) {
            status = danger;
            ratio = 'Н/З';
            message = 'Компанія не платила дивіденди останні 20 років';
        } else if (!hadLastYearPayment) {
            status = danger;
            ratio = 'Н/З';
            message = 'Компанія наразі не виплачує дивiденди'
        } else if (hadLastYearPayment && yearsWithDividends.length < IDEAL_DIVIDENDS_PAYMENT_RANGE) {
            status = warning;
            ratio = previousYear;
            message = 'Компанія наразі виплачує дивiденди';
        } else if (yearsWithDividends.length === IDEAL_DIVIDENDS_PAYMENT_RANGE) {
            status = success;
            ratio = '✔';
            message = 'Компанія платить дивіденди останні 20 років';
        }

        return { status, ratio, message, title, key };
    }

    static getEarningsGrowth(earningsGrowth) {
        const title = 'Зріст прибутку';
        const key = 'earningsGrowth';
        let status;
        let message;
        let ratio;

        const { averageFromPeriodEnd, averageFromPeriodStart } = earningsGrowth;
        const earningsGrowthPercent = ((averageFromPeriodEnd * 100 / averageFromPeriodStart) - 100).toFixed(2);
        const MINIMUM_ACCEPTABLE_GROWTH = 33; // percents

        if (averageFromPeriodEnd <= 0 || averageFromPeriodStart <= 0) {
            status = danger;
            message = averageFromPeriodStart <= 0 
                ? 'На початку десятирічного періоду був зафіксований збиток'
                : 'Наприкінці десятирічного періоду був зафіксований збиток';
            ratio = '✕';
        }

        else if (averageFromPeriodStart >= averageFromPeriodEnd) {
            status = danger;
            message = `Прибутки компанії впали на ${earningsGrowthPercent}% за останні 10 років`;
        } else if (earningsGrowthPercent < MINIMUM_ACCEPTABLE_GROWTH) {
            status = warning;
            message = `Прибутки компанії зросли на ${earningsGrowthPercent}% за останні 10 років`;
        } else {
            status = success;
            message = `Прибутки компанії зросли на ${earningsGrowthPercent}% за останні 10 років`;
        }

        return { status, ratio: ratio || `${earningsGrowthPercent}%`, message, title, key };
    }

    static getPERatio(PERatio) {
        let status;
        let message;

        const IDEAL_VALUE = 8;
        const MAXIMUM_ACCEPTABLE_VALUE = 25;

        if (PERatio > MAXIMUM_ACCEPTABLE_VALUE) {
            status = danger;
            message = 'Відношення ціни акції до прибутку занадто високе'
        } else if (PERatio > IDEAL_VALUE) {
            status = warning;
            message = 'Відношення ціни акції до прибутку задовільне'
        } else {
            status = success;
            message = 'Рекомендоване значення ціни акциї до прибутку'
        }

        return { status, ratio: PERatio, message }
    }

    static getPBRatio(PBRatio) {
        let status;
        let message;

        const IDEAL_VALUE = 1.5;
        const MAXIMUM_ACCEPTABLE_VALUE = 2;

        if (PBRatio > MAXIMUM_ACCEPTABLE_VALUE) {
            status = danger;
            message = 'Відношення ціни акції до балансової вартості занадто високе'
        } else if (PBRatio > IDEAL_VALUE) {
            status = warning;
            message = 'Відношення ціни акції до балансової вартості задовільне'
        } else {
            status = success;
            message = 'Рекомендоване значення ціни акциї до балансової вартості'
        }

        return { status, ratio: PBRatio, message };
    }

    static getGrahamRatio(GrahamRatio, lastUpdatedPrice) {
        const title = 'Коефіцієнт Грема';
        const key = 'GrahamRatio';
        let status;
        let message;

        const MAX_IDEAL_RANGE_VALUE = 70;
        const MIN_IDEAL_RANGE_VALUE = 50;
        const MIN_ACCEPTABLE_RANGE_VALUE = 40;
        const MAX_ACCEPTABLE_RANGE_VALUE = 80;

        const pricePercentageFromGrahamRatio = Number(lastUpdatedPrice * 100 / GrahamRatio);

        if (GrahamRatio === 'Н/З') {
            status = secondary;
            message = 'Недостатньо інформації длНегая розрахунку';
        } else if (GrahamRatio < 0) {
            status = danger;
            message = 'Компанія має забагато довгострокових боргів';
        } else if (pricePercentageFromGrahamRatio < MIN_ACCEPTABLE_RANGE_VALUE) {
            status = danger;
            message = 'Коефіцієнт Грема занадто високий. Задовільний показник має бути в діапазоні 40-80% від поточної ціни акції.'
        } else if (pricePercentageFromGrahamRatio > MAX_ACCEPTABLE_RANGE_VALUE) {
            status = danger;
            message = 'Коефіцієнт Грема занадто низький. Задовільний показник має бути в діапазоні 40-80% від поточної ціни акції.'
        } else if (
            pricePercentageFromGrahamRatio >= MIN_IDEAL_RANGE_VALUE && 
            pricePercentageFromGrahamRatio <= MAX_IDEAL_RANGE_VALUE
        ) {
            status = success;
            message = 'Рекомендоване значення коефіцієнта Грема'
        } else {
            status = warning;
            message = 'Задовільне значення коефіцієнту Грема. Рекомендований показник має бути в діапазоні 50-70% від поточної ціни акції.';
        }

        return { status, ratio: GrahamRatio, message, title, key };
    }

    static getGrahamNumber(PE, PB, GrahamNumber, lastUpdatedPrice) {
        const title = 'Число Грема';
        const key = 'GrahamNumber';
        const MAX_ACCEPTED_RATIO = 22.5
        let status;
        let message;
        console.log(+GrahamNumber, +lastUpdatedPrice, +GrahamNumber > +lastUpdatedPrice);

        if (PE * PB > MAX_ACCEPTED_RATIO) {
            status = danger;
            message = 'Множина коефіцієнтів P/B та P/E перевищує максимально можливе значення';
        } else if (+GrahamNumber < +lastUpdatedPrice) {
            status = danger;
            message = 'Акція переоцінена';
        } else {
            status = success;
            message = 'Акція недооцінена';
        }

        return { status, ratio: GrahamNumber, message, title, key };
    }
}