import {
    getOverview,
    getIncomeStatement,
    getMonthlySummary,
    getBalanceSheet,
    getEarnings,
    getLastUpdatedPrice,
} from '@/api';

import RatioService from './Ratio';
import StatusService from './Status';

export default class StockMapper {
    constructor(symbol) {
        this.symbol = symbol.toUpperCase();
    }

    async initialize() {
        const [
            overview,
            incomeStatement,
            monthlySummary,
            balanceSheet,
            earnings,
            lastUpdatedPrice
        ] = await Promise.all([
            getOverview(this.symbol),
            getIncomeStatement(this.symbol),
            getMonthlySummary(this.symbol),
            getBalanceSheet(this.symbol),
            getEarnings(this.symbol),
            getLastUpdatedPrice(this.symbol),
        ])

        this.name = overview.Name;
        this.industry = overview.Industry;
        this.lastUpdatedPrice = lastUpdatedPrice;

        //TODO delete after testing
        this.overview = overview;
        this.incomeStatement = incomeStatement;
        this.monthlySummary = monthlySummary;
        this.balanceSheet = balanceSheet;
        this.earnings = earnings;

        this.earningsByEachQuarter    = RatioService.getEarningsByQuarter(incomeStatement.quarterlyReports);
        this.dividendsByEachYear      = RatioService.getDividendHistory(monthlySummary['Monthly Adjusted Time Series']);
        this.earningsGrowth           = RatioService.getEarningsGrowth(earnings.annualEarnings);
        this.PERatio                  = +overview.PERatio;
        this.PBRatio                  = +overview.PriceToBookRatio;

        const currentRatio            = RatioService.getCurrentRatio(balanceSheet);
        const lastUnprofitableQuarter = RatioService.getLastUnprofitableQuarter(incomeStatement.quarterlyReports);
        const lastUnprofitableYear    = RatioService.getLastUnprofitableYear(incomeStatement.annualReports);
        const GrahamRatio             = RatioService.getGrahamRatio(balanceSheet, overview.SharesOutstanding);
        // check McDonalds, Goldman Sachs
        const GrahamNumber            = RatioService.getGrahamNumber(+this.overview.EPS, +this.overview.BookValue);

        this.ratios = [
            StatusService.getLastUpdatedPrice(this.lastUpdatedPrice),
            StatusService.getCurrentRatio(currentRatio),
            StatusService.getStableEarnings(lastUnprofitableYear, lastUnprofitableQuarter),
            StatusService.getDividendHistory(this.dividendsByEachYear),
            StatusService.getEarningsGrowth(this.earningsGrowth),
            StatusService.getGrahamRatio(GrahamRatio, this.lastUpdatedPrice.price,),
            StatusService.getGrahamNumber(this.PERatio, this.PBRatio, GrahamNumber, this.lastUpdatedPrice.price),
        ];

        return this;
    }
}