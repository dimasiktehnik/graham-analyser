export function getAverageValueByKey(arr, key) {
    const sum = arr.reduce((sum, item) => { 
        return item[key] > 0 ? sum + item[key] : sum;
    }, 0);
    const averageValue = sum / arr.length;

    return averageValue;
}

export function syncFavoritesStocksWithLocalStorage(key, value) {
    localStorage.setItem(key, value);
}

export function formatNumbersToMillions(value) {
    const sign = Math.sign(Number(value));

    return sign * (Math.abs(Number(value)) / 1.0e6)
}

export function formatQuarter(date) {
    const [ year, monthNumber ] = date.split('-');
    const monthName = getMonthName(+monthNumber - 1);

    return `${monthName} of ${year}`;
}

function getMonthName(number) {
    const date = new Date();
    date.setMonth(number - 1);
    
    return date.toLocaleString('en-US', { month: 'short' });
}